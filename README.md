# Manuscript title:
# Altered astrocytic and microglial homeostasis characterizes a decreased proinflammatory state in bipolar disorder
# 
# Authors:
# Quentin Amossé, Benjamin B. Tournier, Aurélien M. Badina, Lilou Marchand-Maillet, Laurene Abjean, Sylvain Lengacher, Nurun Fancy, Amy M. Smith, Yeung-Yeung Leung, Verena Santer, Valentina Garibotto, David R. Owen, Camille Piguet, Kelly Ceyzériat, Stergios Tsartsalis and Philippe Millet
# 
# script authors: 
# Quentin Amossé, Stergios Tsartsalis, Nurun Fancy
# 
# 
# Scripts primarily consist of R code or .sh code executed on an HPC cluster
