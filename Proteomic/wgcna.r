run_wgcna <- function(expr_data,
                      soft_power,
                      cor_func = c("cor", "bicor"),
                      cor_method = c("pearson", "spearman"),
                      network_type = c("unsigned", "signed hybrid"),
                      min_module_size = 30,
                      merge_module = FALSE,
                      merge_dist = 0.2,
                      deep_split = 1,
                      cut_height = NULL
){
  
  t0 <- Sys.time()
  
  library(WGCNA)
  library(RColorBrewer) 
  library(ggplot2)
  library(dplyr)
  library(cli)
  
  # transposing the expression matrix
  expr_data_t <- expr_data %>% t()
  
  # calculating the adjacency matrix
  
  cli::cli_text("Calculating adjacency matrix...")
  
  if (cor_func == "cor"){
    adjacency = adjacency(expr_data_t, 
                          power = soft_power, 
                          type = network_type, 
                          corFnc = cor_func,
                          corOptions = list(use = "p", method = cor_method) )
  } else if (cor_func == "bicor") {
    adjacency = adjacency(expr_data_t, 
                          power = soft_power, 
                          type = network_type, 
                          corFnc = cor_func)
  }
  
  # calculating the topological-overlap matrix
  
  cli::cli_text("Calculating topological overlap matrix...")
  
  tom = TOMsimilarity(adjacency)
  diss_tom = 1-tom
  
  # clustering the tom distance
  gene_tree = hclust(as.dist(diss_tom), method = "average")
  
  cli::cli_text("Constructing modules...")
  
  # cutting gene tree to construct modules
  dynamic_modules = cutreeDynamic(dendro = gene_tree, 
                                  distM = diss_tom,
                                  cutHeight = cut_height,
                                  minClusterSize = min_module_size,
                                  deepSplit = deep_split)
  
  
  # Convert numeric labels into colors
  module_colors = labels2colors(dynamic_modules)
  
  if(merge_module) {
    ME_list = moduleEigengenes(expr_data_t, module_colors)
    ME = ME_list$eigengenes
    colnames(ME) <- gsub("ME", "", colnames(ME))
    ME_diss = 1-cor(ME)
    ME_tree = hclust(as.dist(ME_diss), method = "average")
    #plot(ME_tree, main = "Clustering of module eigengenes", xlab = "", sub = "")
    ME_diss_thresh = merge_dist #corresponds to a correlation of 0.80
    #abline(h=ME_diss_thresh, col = "red")
    merge = mergeCloseModules(expr_data_t, 
                              module_colors, 
                              cutHeight = ME_diss_thresh, 
                              verbose = 3)
    merged_colors = merge$colors
    merged_ME = merge$newMEs
    # plotDendroAndColors(gene_tree, cbind(module_colors, merged_colors),
    #                     c("Dynamic Tree Cut", "Merged dynamic"),
    #                     dendroLabels = FALSE, hang = 0.03,
    #                     addGuide = TRUE, guideHang = 0.05)
    module_colors <- merged_colors
  }
  
  
  
  # Construct numerical labels corresponding to the colors
  color_order = c("grey", standardColors(50))
  module_labels = match(module_colors, color_order)-1
  module_labels = paste0("M", module_labels)
  
  # # Calculate MEs with color labels
  # ME_list = moduleEigengenes(expr_data_t, module_colors)
  # ME = ME_list$eigengenes %>% orderMEs()
  
  cli::cli_text("Calculating module eigengene...")
  
  # Calculate MEs with module labels
  ME_list = moduleEigengenes(expr_data_t, module_labels)
  ME = ME_list$eigengenes %>% orderMEs()
  colnames(ME) <- gsub("ME", "", colnames(ME))
  
  # generating module gene list
  modules <- names(table(module_labels))
  
  module_list <- list()
  for(i in modules){
    module_list[[i]] <- colnames(expr_data_t)[module_labels==i]
  }
  
  module_list[["background"]] = rownames(expr_data)
  
  # calculating gene module membership
  nsamples = nrow(expr_data_t)
  gene_module_membership = as.data.frame(cor(expr_data_t, 
                                             ME, 
                                             use = "p", 
                                             method = "spearman")) #same as signedKme
  
  gene_MMPvalue = as.data.frame(corPvalueStudent(as.matrix(gene_module_membership), nsamples))
  
  colnames(gene_module_membership) = paste("MM", colnames(gene_module_membership), sep="_")
  colnames(gene_MMPvalue) = paste("p.MM",  colnames(gene_MMPvalue), sep="_")
  
  
  # calculating top hub genes in each module
  
  top_hubs <- chooseTopHubInEachModule(
    datExpr = expr_data_t, 
    colorh = module_labels, 
    omitColors = "M0", 
    power = soft_power, 
    type = network_type)      
  
  # Exporting network for cytoscape
  cli::cli_text("Exporting network for visualisation...")
  
  colnames(tom) <- colnames(expr_data_t)
  rownames(tom) <- colnames(expr_data_t)
  
  network_params <- list(nsample = ncol(expr_data), 
                         ngenes = nrow(expr_data),
                         soft_power = soft_power, 
                         cor_func = cor_func, 
                         cor_method = ifelse(cor_func == "cor", cor_method, "None"), 
                         network_type = network_type,
                         min_module_size = min_module_size,
                         merge_module = merge_module,
                         merge_dist = merge_dist,
                         cut_height = cut_height,
                         deep_split = deep_split
                         )
  
  network_params <- unlist(network_params)
  
  gc()
  
  t1 <- Sys.time()
  
  time_spent <- round(t1-t0, 2)
  
  cat(paste("The analysis took", time_spent, "seconds", sep = " "))
  
  return(invisible(list(adjacency = adjacency,
                        tom = tom,
                        diss_tom = diss_tom,
                        module_labels = module_labels,
                        modules = as.data.frame(table(module_labels)),
                        module_list = module_list, 
                        module_avg_expr = ME_list$averageExpr,
                        module_eigengenes = ME_list$eigengenes,
                        gene_module_membership = gene_module_membership,
                        gene_MMPvalue = gene_MMPvalue,
                        top_hubs = top_hubs,
                        network_params = network_params)))
}
