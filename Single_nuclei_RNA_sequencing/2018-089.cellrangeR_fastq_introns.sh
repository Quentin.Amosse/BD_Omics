#!/bin/sh
#SBATCH --job-name cellranger2018-089
#SBATCH --error cellranger2018-089-error.e%j
#SBATCH --output cellranger2018-089-out.o%j
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 16


#SBATCH --partition public-cpu
#SBATCH --time 0-12:00:00
#SBATCH --mail-type=ALL

module load CellRanger
cellranger count --id=2018-089_out_introns \
                 --transcriptome=/home/users/q/quentina/data/snRNAseq/references/refdata-gex-GRCh38-2020-A \
                 --fastqs=/home/share/milletp/snRNAseq/20210325/2018-089 \
                 --include-introns