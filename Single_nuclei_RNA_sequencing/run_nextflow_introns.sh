#!/bin/sh

cd ~/scratch/

module load GCC/6.3.0-2.27 Singularity/2.4.2

/home/users/q/quentina/data/nextflow/nextflow run /home/users/q/quentina/data/nextflow/scflow/main.nf --samplesheet /home/users/q/quentina/data/nextflow/refs/SampleSheet.tsv --manifest /home/users/q/quentina/data/nextflow/refs/Manifest.txt -c /home/users/q/quentina/data/nextflow/conf/unige.config -c /home/users/q/quentina/data/nextflow/conf/scflow_analysis.config -c /home/users/q/quentina/data/nextflow/conf/resource_general.config -resume
