#!/bin/sh
#SBATCH --job-name cellranger2018-067
#SBATCH --error cellranger2018-067-error.e%j
#SBATCH --output cellranger2018-067-out.o%j
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 16


#SBATCH --partition public-cpu
#SBATCH --time 0-12:00:00
#SBATCH --mail-type=ALL

module load CellRanger
cellranger count --id=2018-067_out_introns \
                 --transcriptome=/home/users/q/quentina/data/snRNAseq/references/refdata-gex-GRCh38-2020-A \
                 --fastqs=/home/share/milletp/snRNAseq/20210319/2018-067 \
                 --include-introns