#!/bin/sh
#SBATCH --job-name cellranger2018-042
#SBATCH --error cellranger2018-042-error.e%j
#SBATCH --output cellranger2018-042-out.o%j
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 16


#SBATCH --partition public-cpu
#SBATCH --time 0-18:00:00
#SBATCH --mail-type=ALL

module load CellRanger
cellranger count --id=2018-042_out_introns \
                 --transcriptome=/home/users/q/quentina/data/snRNAseq/references/refdata-gex-GRCh38-2020-A \
                 --fastqs=/home/share/milletp/snRNAseq/20210311/2018-042 \
                 --include-introns