#!/bin/sh
#SBATCH --job-name integration_reddim_clustering_Micro
#SBATCH -o /home/users/q/quentina/data/subclusters/micro-error.o%j
#SBATCH -e /home/users/q/quentina/data/subclusters/micro-out.e%j

#SBATCH --ntasks 1
#SBATCH --cpus-per-task 6
#SBATCH --partition public-cpu

#SBATCH --time 0-00:30:00
#SBATCH --mail-type=ALL

module load GCC/9.3.0

module load Singularity/3.7.3-Go-1.14

cd /home/users/q/quentina/scratch/




START=$(date)
echo "Starting integration at" $START

singularity exec -B /home/,/home/users/q/quentina/scratch/:/tmp,/home/,/home/users/q/quentina/scratch/:/var/tmp,/home/,/home/users/q/quentina/data/:/data/ \
/home/users/q/quentina/data/Nextflow/singularity-cache-new/combiz-scflow-docker-0.7.0.img \
Rscript /home/users/q/quentina/data/Nextflow/scflow/bin/scflow_integrate.r --method Liger --unique_id_var manifest --take_gene_union false --remove_missing true --num_genes 2000 --combine union --keep_unique false --capitalize false --use_cols true --k 40 --lambda 3.0 --thresh 0.0001 --max_iters 100 --nrep 1 --rand_seed 1 --knn_k 10 --k2 500 --prune_thresh 0.2 --quantiles 50 --ref_dataset NULL --min_cells 2 --center false  --nstart 10  --resolution 1 --dims_use NULL --dist_use CR --small_clust_thresh 0 --sce_path /home/users/q/quentina/data/subclusters/Micro/micro_sce


END=$(date)
echo "Integration ended at" $END

START=$(date)
echo "Starting dimension reduction at" $START

singularity exec -B /home/,/home/users/q/quentina/scratch/:/tmp,/home/,/home/users/q/quentina/scratch/:/var/tmp,/home/,/home/users/q/quentina/data/:/data/ \
/home/users/q/quentina/data/Nextflow/singularity-cache-new/combiz-scflow-docker-0.7.0.img \
Rscript /home/users/q/quentina/data/Nextflow/scflow/bin/scflow_reduce_dims.r --sce_path /home/users/q/quentina/data/subclusters/Micro/integrated_sce  --input_reduced_dim PCA,Liger --reduction_methods UMAP --vars_to_regress_out nCount_RNA,pc_mito --pca_dims 30 --n_neighbors 35 --n_components 2 --init spectral --metric euclidean --n_epochs 200 --learning_rate 1 --min_dist 0.4 --spread 0.85 --set_op_mix_ratio 1 --local_connectivity 1 --repulsion_strength 1 --negative_sample_rate 5 --fast_sgd FALSE --dims 2 --initial_dims 50 --perplexity 150 --theta 0.5 --max_iter 1000 --pca_center TRUE --pca_scale FALSE --normalize TRUE --stop_lying_iter 250 --mom_switch_iter 250  --momentum 0.5 --final_momentum 0.8 --eta 1000 --exaggeration_factor 12

END=$(date)
echo "Dimension reduction ended at" $END

START=$(date)
echo "Starting clustering at" $START

singularity exec -B /home/,/home/users/q/quentina/scratch/:/tmp,/home/,/home/users/q/quentina/scratch/:/var/tmp,/home/,/home/users/q/quentina/data/:/data/ \
/home/users/q/quentina/data/Nextflow/singularity-cache-new/combiz-scflow-docker-0.7.0.img \
Rscript /home/users/q/quentina/data/Nextflow/scflow/bin/scflow_cluster.r --sce_path /home/users/q/quentina/reddim_sce --cluster_method leiden --reduction_method UMAP_Liger --res 0.001 --k 50 --louvain_iter 1

END=$(date)
echo "Clustering ended at" $END

