#!/bin/sh

#SBATCH --job-name glmer_diagnosis_qsub_Micro
#SBATCH --error /home/users/q/quentina/data/mast/glmer_diagnosis_qsub_Micro-error.e%j
#SBATCH --output /home/users/q/quentina/data/mast/glmer_diagnosis_qsub_Micro-out.o%j
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 8


#SBATCH --partition public-cpu
#SBATCH --time 0-06:00:00
#SBATCH --mail-type=ALL

module load GCC/9.3.0

module load Singularity/3.7.3-Go-1.14

cd /home/users/q/quentina/data/mast/

input_dir=/home/users/q/quentina/data/mast/
input_sce=micro_sce.qs
input_dir_container=/home/users/q/quentina/data/mast/

START=$(date)
echo job started at $START

singularity exec -B /home/,/home/users/q/quentina/:/tmp,/home/users/q/quentina/:/var/tmp,/srv/beegfs/scratch/users/t/tsartsa0/:/tmp,/srv/beegfs/scratch/users/q/quentina/:/var/tmp \
/home/users/q/quentina/data/nextflow/singularity-cache-new/combiz-scflow-docker-0.7.0.img \
Rscript /home/users/q/quentina/data/mast/run_de_diagnosis_glmer-batch.r \
--sce $input_dir_container/$input_sce

END=$(date)
echo job ended at $END
