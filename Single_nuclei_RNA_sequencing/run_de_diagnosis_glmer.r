#!/usr/bin/env Rscript
# Perform differential expression analysis 
# Nurun Fancy <n.fancy@imperial.ac.uk>

#   ____________________________________________________________________________
#   Initialization                                                          ####

##  ............................................................................
##  Load packages                                                           ####
library(SingleCellExperiment)
library(scater)
library(dplyr)
library(purrr)
library(argparse)
library(scFlow)
library(qs)

##  ............................................................................
##  Parse command-line arguments                                            ####

# create parser object
parser <- ArgumentParser()

# specify options
required <- parser$add_argument_group("Required", "required arguments")
optional <- parser$add_argument_group("Optional", "required arguments")

required$add_argument(
  "--sce",
  help = "path to sce.qs object",
  metavar = "/dir/sce/sce.qs",
  required = TRUE
)

# get command line options, if help option encountered print help and exit,
# otherwise if options not found on command line then set defaults
args <- parser$parse_args()

# set working directory
setwd("/home/users/q/quentina/data/mast")
ensembl_fp <- "/home/users/q/quentina/data/mast/Mappings.tsv" 

dir.create("de")

sce <- qs::qread(args$sce)
#sce <- sce[, sce$clusters %in% names(which(table(sce$clusters) > 100))]

#starting differential expression on variable
contrast <- "diagnosis"



celltype <- gsub("_sce", "", tools::file_path_sans_ext(basename(args$sce)))
confounders <- c("cngeneson",
                 "pc_mito",'sex','batch')
random_effects_var<-'manifest'
unique_id_var<-'manifest'
ref_class='CT'
subset_var = "diagnosis"
subset_class = "CT"
outdir_name <- sprintf("%s/%s_%s/%s", "de", contrast, "glmer_mod1", celltype)
dir.create(outdir_name, recursive = TRUE)

for (region in c("all", "SSC", "EC", "mTemp")) {
    if(region != "all") {
      sce_subset <- sce[, sce$Region == region]
      confounders <- confounders
    } else {
      sce_subset <- sce
      confounders <- confounders
    }

    cli::cli_text(
    "Starting differential expression analysis on {celltype} cells and {region} brain region")
    
    
    de_results <- perform_de(
      sce = sce_subset,
      de_method = "MASTZLM",
      ebayes = FALSE,
      mast_method = "glmer",
      min_counts = 1,
      min_cells_pc = 0.10,
      rescale_numerics = T,
      dependent_var = contrast,
      ref_class = ref_class,
      confounding_vars = confounders,
      random_effects_var = random_effects_var,
      ensembl_mapping_file = ensembl_fp,
      unique_id_var = unique_id_var,
      subset_var = subset_var,
      subset_class = subset_class,
      nAGQ = 0
    )
    
    de_results <- lapply(de_results, function(dt){dt <- dt %>%
    as.data.frame() %>%
    filter(gene_biotype  == "protein_coding")})

    file_name <- sprintf("%s_%s_%s_%s_%s_glmer.tsv", celltype, region, contrast, paste(confounders, collapse = '_'),paste('random_effect',random_effects_var,sep = '_'))
    write.table(
      de_results[[1]],
      file.path(outdir_name, file_name),
      col.names = TRUE, row.names = FALSE, sep = "\t")
  }




